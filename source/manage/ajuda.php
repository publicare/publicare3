<?php
/**
 * Publicare - O CMS Público Brasileiro
 * @description Arquivo
 * @copyright GPL © 2007
 * @package publicare
 *
 * Este arquivo é parte do programa Publicare
 * Publicare é um software livre; você pode redistribuí-lo e/ou modificá-lo dentro dos termos da Licença Pública Geral GNU 
 * como publicada pela Fundação do Software Livre (FSF); na versão 3 da Licença, ou (na sua opinião) qualquer versão.
 * Este programa é distribuído na esperança de que possa ser  útil, mas SEM NENHUMA GARANTIA; sem uma garantia implícita 
 * de ADEQUAÇÃO a qualquer MERCADO ou APLICAÇÃO EM PARTICULAR. Veja a Licença Pública Geral GNU para maiores detalhes.
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU junto com este programa, se não, veja <http://www.gnu.org/licenses/>.
 */
global $_page;
?>

<!-- === Ajuda do Publicare === -->
<div class="panel panel-primary">
    <div class="panel-heading"><h3><b>Ajuda do Publicare</b></h3></div>
    <div class="panel-body">
        <a href="https://manual.publicare.org" target="_blank">
            <u>Programa de ajuda para Editores e Autores</u></a>
        <br><br>
        clique no link para abrir a ajuda.
    </div>
    <div class="panel-footer" style="text-align: right">
        <a href="#" onclick="history.back()" class="btn btn-success">Voltar</a>
    </div>
</div>
<!-- === Final ===  Ajuda do Publicare === -->
